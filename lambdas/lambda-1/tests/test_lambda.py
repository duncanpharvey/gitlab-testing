from datetime import datetime
import sys
import os

current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
import app # noqa: E402

def test_date():
    result = app.parse_date_from_string("Today is January 1, 2047 at 8:21:00AM")
    assert result == datetime(2047, 1, 1, 8, 21, 0)
