from dateutil import parser

def parse_date_from_string(str):
    return parser.parse(str, fuzzy=True)

result = parse_date_from_string("Today is January 1, 2047 at 8:21:00AM")
print(result)
