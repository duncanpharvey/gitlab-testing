import os
import re
import sys

CI_COMMIT_BRANCH = sys.argv[1]
CI_DEFAULT_BRANCH = sys.argv[2]

# PYTHON_IMAGE = 'registry.ddbuild.io/images/mirror/python:3.9'
PYTHON_IMAGE = 'python:3.9'

ROLE_PREFIX = 'arn:aws:iam::133132990048:role/'

DEV_PIPELINE_EXECUTION_ROLE = ROLE_PREFIX + 'lambda-pipeline-dev-PipelineExecutionRole-3KGV4DT3QLF2'
DEV_CLOUDFORMATION_EXECUTION_ROLE = ROLE_PREFIX + 'lambda-pipeline-dev-CloudFormationExecutionRole-Q9KY146X4ZJJ'
DEV_ARTIFACTS_BUCKET = 'lambda-pipeline-dev-artifactsbucket-ksgnaplrwnck'

PROD_PIPELINE_EXECUTION_ROLE = ROLE_PREFIX + 'lambda-pipeline-prod-PipelineExecutionRole-9365WFBWQHOY'
PROD_CLOUDFORMATION_EXECUTION_ROLE = ROLE_PREFIX + 'lambda-pipeline-prod-CloudFormationExecutionRole-1SWYSSUOAL1GW'
PROD_ARTIFACTS_BUCKET = 'lambda-pipeline-prod-artifactsbucket-rn4skam8txix'

deploy = False
if CI_COMMIT_BRANCH == CI_DEFAULT_BRANCH:
    deploy = True
    env = 'prod'
    pipeline_execution_role = PROD_PIPELINE_EXECUTION_ROLE
    cloudformation_execution_role = PROD_CLOUDFORMATION_EXECUTION_ROLE
    artifacts_bucket = PROD_ARTIFACTS_BUCKET
elif CI_COMMIT_BRANCH == 'dev':
    deploy = True
    env = 'dev'
    pipeline_execution_role = DEV_PIPELINE_EXECUTION_ROLE
    cloudformation_execution_role = DEV_CLOUDFORMATION_EXECUTION_ROLE
    artifacts_bucket = DEV_ARTIFACTS_BUCKET


with open(os.path.join('ci/jobs/lint.yml'), 'r') as lint_job_file:
    lint_job = lint_job_file.read()

with open(os.path.join('ci/jobs/test.yml'), 'r') as test_job_file:
    test_job = test_job_file.read()

with open(os.path.join('ci/jobs/delete.yml'), 'r') as delete_job_file:
    delete_job = delete_job_file.read()

with open(os.path.join('ci/jobs/deploy.yml'), 'r') as deploy_job_file:
    deploy_job = deploy_job_file.read()


def directory_has_template(dirname):
    return any([filename == 'template.yaml' for filename in os.listdir(dirname)])


def get_changed_lambdas():
    with open('tmp/changed-files.txt', 'r') as changed_files:
        changed_dirs_with_template = set()
        for line in changed_files:
            path = line.strip()
            result = re.search(r'(^lambdas/[^/]+/).+', path)
            if not result:
                print(f'{path} not related to lambdas')
                continue
            lambda_path = result.group(1)
            dirname = os.path.dirname(lambda_path)
            if os.path.isdir(dirname) and directory_has_template(dirname):
                base_dirname = os.path.basename(dirname)
                changed_dirs_with_template.add(base_dirname)

    return changed_dirs_with_template


def get_deleted_lambdas():
    with open('tmp/deleted-files.txt', 'r') as deleted_files:
        deleted_templates = set()
        for line in deleted_files:
            path = line.strip()
            filename = os.path.basename(path)
            if filename == 'template.yaml':
                dirname = os.path.dirname(path)
                base_dirname = os.path.basename(dirname)
                deleted_templates.add(base_dirname)

    return deleted_templates


changed_lambdas = get_changed_lambdas()
deleted_lambdas = get_deleted_lambdas()

# deleting takes preference over changes (this is needed if a template.yaml is deleted but other files in the directory are changed)
changed_lambdas.difference_update(deleted_lambdas)

print('changed lambdas', changed_lambdas)
print('deleted lambdas', deleted_lambdas)

config = ''

for changed_lambda in changed_lambdas:
    config += lint_job.replace('{{lambda}}', changed_lambda) \
                    .replace('{{image}}', PYTHON_IMAGE)

    tests_path = os.path.join('lambdas', changed_lambda, 'tests')
    if os.path.exists(tests_path) and len(os.listdir(tests_path)):
        config += test_job.replace('{{lambda}}', changed_lambda) \
                    .replace('{{image}}', PYTHON_IMAGE)

    if deploy:
        config += deploy_job.replace('{{lambda}}', changed_lambda) \
                    .replace('{{env}}', env) \
                    .replace('{{region}}', 'us-east-1') \
                    .replace('{{image}}', PYTHON_IMAGE) \
                    .replace('{{pipeline_execution_role}}', pipeline_execution_role) \
                    .replace('{{cloudformation_execution_role}}', cloudformation_execution_role) \
                    .replace('{{artifacts_bucket}}', artifacts_bucket)


for deleted_lambda in deleted_lambdas:
    if deploy:
        config += delete_job.replace('{{lambda}}', deleted_lambda) \
                    .replace('{{env}}', env) \
                    .replace('{{region}}', 'us-east-1') \
                    .replace('{{image}}', PYTHON_IMAGE) \
                    .replace('{{pipeline_execution_role}}', pipeline_execution_role)


if not config:
    config = f'''
empty-job:
  image: {PYTHON_IMAGE}
  # tags: ["runner:main"]
  script:
    - echo "no lambdas jobs to generate"
'''
else:
    config = '''
stages:
  - lint
  - test
  - deploy

''' + config

with open('tmp/config.yml', 'w') as f:
    f.write(config)
