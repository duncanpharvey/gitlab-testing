#!/bin/bash
mkdir tmp

if [ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ] || [ $CI_COMMIT_BRANCH == 'dev' ];
then
    git diff --name-only --no-renames --diff-filter=d $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA > tmp/changed-files.txt;
    git diff --name-only --no-renames --diff-filter=D $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA > tmp/deleted-files.txt;
else
    git diff --name-only --no-renames --diff-filter=d `git merge-base origin/$CI_DEFAULT_BRANCH $CI_COMMIT_SHA` $CI_COMMIT_SHA > tmp/changed-files.txt;
    git diff --name-only --no-renames --diff-filter=D `git merge-base origin/$CI_DEFAULT_BRANCH $CI_COMMIT_SHA` $CI_COMMIT_SHA > tmp/deleted-files.txt;
fi

cat tmp/changed-files.txt
cat tmp/deleted-files.txt
